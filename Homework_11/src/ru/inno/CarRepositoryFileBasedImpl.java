package ru.inno;

import java.io.*;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarRepositoryFileBasedImpl implements CarRepository {

    private final String fileName;

    public CarRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Car> stringToCarMapper = currentCar -> {
        String[] parts = currentCar.split("\\|");
        String carNumber = parts[0];
        String model = parts[1];
        String color = parts[2];
        double mileage = Double.parseDouble(parts[3]);
        int price = Integer.parseInt(parts[4]);
        return new Car(carNumber, model, color, mileage, price);
    };

    private static final Function<Car, String> carToStringMapper = car -> car.getCarNumber() + "|"
            + car.getModel() + "|"
            + car.getColor() + "|"
            + car.getMileage() + "|"
            + car.getPrice();

    @Override
    public List<Car> findAll() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileExceptions(e);
        }
    }

    @Override
    public void save(Car car) {
        try (FileWriter fileWriter = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            String carToSave = carToStringMapper.apply(car);
            bufferedWriter.write(carToSave);
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileExceptions(e);
        }

    }

    @Override
    public List<String> findNumberByColorOrMileage(String color, double mileage) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getColor().equals(color) || car.getMileage() == mileage)
                    .map(Car::getCarNumber)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileExceptions(e);
        }
    }

    @Override
    public long findAllUniqueCarInRangeOfPrice(int priceFrom, int priceTo) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getPrice() >= priceFrom && car.getPrice() <= priceTo)
                    .distinct()
                    .count();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileExceptions(e);
        }
    }

    @Override
    public String findColorByMinimalPrice() {
        try {
            Car car = new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .min(Comparator.comparingInt(Car::getPrice))
                    .get();
            return car.getColor();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileExceptions(e);
        }
    }

    @Override
    public double findAveragePriceOfCar(String model) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getModel().equals(model))
                    .mapToInt(Car::getPrice)
                    .average()
                    .getAsDouble();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileExceptions(e);
        }
    }
}
