package ru.inno;

public class Main {
    public static void completeAllTask(Task[] tasks) {
        for (Task task : tasks) {
            task.complete();
        }
    }

    public static void main(String[] args) {

        EvenNumbersPrintTask e1 = new EvenNumbersPrintTask(0, 12);
        EvenNumbersPrintTask e2 = new EvenNumbersPrintTask(18, 25);
        EvenNumbersPrintTask e3 = new EvenNumbersPrintTask(34, 66);

        OddNumbersPrintTask o1 = new OddNumbersPrintTask(0, 11);
        OddNumbersPrintTask o2 = new OddNumbersPrintTask(14, 37);
        OddNumbersPrintTask o3 = new OddNumbersPrintTask(52, 77);

        Task[] tasks = {e1, e2, e3, o1, o2, o3};
        completeAllTask(tasks);
    }
}