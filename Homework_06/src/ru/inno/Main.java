package ru.inno;

public class Main {
    public static void main(String[] args) {
        Figures figures = new Figures(4, 5);

        Square square = new Square(figures.centerX, figures.centerY, 6);
        square.perimeterSquare();
        square.squareArea();
        square.moveSquare(6,7);
        System.out.println("Периметр квадрата" + square.getPerimeter());
        System.out.println("Площадь квадрата" + square.getArea());

        Rectangle rectangle = new Rectangle(figures.centerX, figures.centerY, square.sideLength, 8);
        rectangle.perimeterRectangle();
        rectangle.areaRectangle();
        rectangle.moveRectangle(9,4);
        System.out.println("Периметр прямоугольника" + rectangle.getPerimeter());
        System.out.println("Площадь прямоугольника" + rectangle.getArea());

        Circle circle = new Circle(figures.centerX, figures.centerY, 5);
        circle.perimeterCircle();
        circle.areaCircle();
        circle.moveCircle(4, 7);
        System.out.println("Периметр круга" + square.getPerimeter());
        System.out.println("Площадь круга" + square.getArea());

        Ellipse ellipse = new Ellipse(figures.centerX, figures.centerY, circle.radius, 11);
        ellipse.perimeterEllipse();
        ellipse.areaEllipse();
        ellipse.moveEllipse(2,5);
        System.out.println("Периметр эллипса" + ellipse.getPerimeter());
        System.out.println("Площадь эллипса" + ellipse.getArea());

    }
}