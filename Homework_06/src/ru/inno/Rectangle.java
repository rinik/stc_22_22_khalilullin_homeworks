package ru.inno;

public class Rectangle extends Square  {
    private final double sideWidth;
    private double perimeter;
    private double area;

    public Rectangle(int x, int y, double sideLength, double sideWidth) {
        super(x, y, sideLength);
        this.sideWidth = sideWidth;
    }
    public double perimeterRectangle () {
        this.perimeter = 2 * (sideWidth + sideLength);
        return perimeter;
    }
    public double areaRectangle () {
        this.area = sideWidth * sideLength;
        return area;
    }
    protected void moveRectangle (int x, int y) {
        this.centerX = x;
        this.centerY = y;
    }
    public double getPerimeter() {
        return perimeter;
    }
    public double getArea() {
        return area;
    }
}
