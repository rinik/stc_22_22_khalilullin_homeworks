package ru.inno;

public class Ellipse extends Circle {
    private final double largeRadius;
    private double perimeter;
    private double area;

    public Ellipse(int x, int y, int radius, int largeRadius) {
        super(x, y, radius);
        this.largeRadius = largeRadius;
    }
    public double perimeterEllipse () {
        perimeter = 4 * (Math.PI*radius*largeRadius+(largeRadius-radius))/(largeRadius+radius);
        return perimeter;
    }
    public double areaEllipse () {
        area = largeRadius * radius * Math.PI;
        return area;
    }
    protected void moveEllipse (int x, int y) {
        this.centerX = x;
        this.centerY = y;
    }
    public double getPerimeter() {
        return perimeter;
    }
    public double getArea() {
        return area;
    }
}
