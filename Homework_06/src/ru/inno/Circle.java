package ru.inno;

public class Circle extends Figures {
    protected int radius;
    private double perimeter;
    private double area;

    public Circle(int x, int y, int radius) {
        super(x, y);
        this.radius = radius;
    }
    public double perimeterCircle () {
        perimeter = 2 * Math.PI * radius;
        return perimeter;
    }
    public double areaCircle () {
        area = Math.PI * radius * radius;
        return area;
    }
    protected void moveCircle(int x, int y) {
        this.centerX = x;
        this.centerY = y;
    }
    public double getPerimeter() {
        return perimeter;
    }
    public double getArea() {
        return area;
    }
}
