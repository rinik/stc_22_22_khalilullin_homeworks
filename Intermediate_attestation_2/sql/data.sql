insert into person (age, have_license)
values (21, true),
       (22, false);

insert into person (age, have_license, experience)
values (22, true, 15);

insert into person (age, have_license, license_category)
values (22, true, 'D');

insert into person (first_name, last_name, phone_number, experience, age, have_license, license_category, rating)
values ('Rinat', 'Khalilullin', '+78007654321', 14, 34, true, 'B', 5);

-------------------------------------------------------------------------------------------------------------------

insert into car (model, color, number, owner_id)
values ('Genesis GV 70', 'blue', 'y874km77', 5);

insert into car (model, number, owner_id)
values ('Hyundai Tucson', 'c543ok152', 1),
       ('Audi Q5', 'k890ek999', 2),
       ('Ford Mustang', 'e454ao770', 3),
       ('Porsche 911', 'o911oo77', 4);

---------------------------------------------------------------------------------------------------------------------

insert into ride (driver_id, car_id, ride_date, ride_interval)
values (5, 1, '2022-11-03', '1:00'),
       (1, 3, '2022-10-10', '0:30'),
       (2, 4, '2022-10-25', '1:25'),
       (3, 4, '2022-11-15', '0:45'),
       (4, 5, '2022-11-12', '2:40');