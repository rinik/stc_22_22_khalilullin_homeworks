public class Main {
    public static void main(String[] args) {
        int[] number = {9,8,7,2,3};
        int result = toInt(number);
        System.out.println(result);
    }
    public static int toInt(int[] a) {
        int s = 0;
        for (int i = 0; i < a.length; i ++) {
            s = 10 * s + a[i];
        }
        return s;
    }
}
