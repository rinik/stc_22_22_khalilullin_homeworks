package ru.inno;

public class Product {
    private Integer number;
    private String nameProduct;
    private Double price;
    private Integer quantityProduct;
    public Product(Integer number, String nameProduct, Double price, Integer quantityProduct) {
        this.number = number;
        this.nameProduct = nameProduct;
        this.price = price;
        this.quantityProduct = quantityProduct;
    }
    public Integer getNumber() {
        return number;
    }
    public void setNumber(Integer number) {
        this.number = number;
    }
    public String getNameProduct() {
        return nameProduct;
    }
    public void setNameProduct(String nameProduct){
        this.nameProduct = nameProduct;
    }
    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price) {
        this.price = price;
    }
    public Integer getQuantityProduct() {
        return quantityProduct;
    }
    public void setQuantityProduct(Integer quantityProduct) {
        this.quantityProduct = quantityProduct;
    }
    public String toString() {
        return "Product{" +
                "number=" + number + '\'' +
                "nameProduct=" + nameProduct + '\'' +
                "price=" + price + '\'' +
                "quantityProduct=" + quantityProduct + '\'' +
                '}' ;
    }
}

