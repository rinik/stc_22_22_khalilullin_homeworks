package ru.inno;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
    ProductsRepository productsRepository = new ProductsRepositoryFileBasedImpl("Products.txt");
        System.out.println(productsRepository.findById(7));
        System.out.println(productsRepository.findAllByTitleLike("оло"));
        Product milk = productsRepository.findById(1);
        milk.setQuantityProduct(20);
        milk.setPrice(50.8);
        productsRepository.update(milk);
        System.out.println(productsRepository.findById(1));
    }
}