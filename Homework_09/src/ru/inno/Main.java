package ru.inno;

import java.util.ListIterator;

public class Main {

    public static String mergeDocuments(Iterable<String> documents) {
        StringBuilder mergedDocument = new StringBuilder();

        Iterator<String> documentsIterator = documents.iterator();

        while (documentsIterator.hasNext()) {
            mergedDocument.append(documentsIterator.next() + " ");
        }

        return mergedDocument.toString();
    }

    public static void main(String[] args) {
        List<Integer> intList = new ArrayList<>();

        intList.add(8);
        intList.add(10);
        intList.add(11);
        intList.add(13);
        intList.add(11);
        intList.add(15);
        intList.add(-5);
        intList.removeAt(2);

        for (int i = 0; i < intList.size(); i++) {
            System.out.println(intList.get(i));
        }


    }
}