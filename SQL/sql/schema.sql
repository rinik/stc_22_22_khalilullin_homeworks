drop table if exists student;
drop table lesson;
drop table if exists course;

create table student (
                         id bigserial primary key,
                         email char(20) unique,
                         password char(100),
                         first_name char(20) default 'DEFAULT_FIRSTNAME',
                         last_name char(20) default 'DEFAULT_LASTNAME',
                         age integer check (age >= 0 and age <= 120) not null,
                         is_worker bool
);

alter table student alter column email set not null;
alter table student add column average double precision check (  average >= 0 and average <= 5 ) default 0;

create table course (
                        id serial primary key,
                        title char(10) not null,
                        description char(500),
                        start timestamp,
                        finish timestamp
);


create table lesson (
                        id serial primary key,
                        name char(20),
                        summary char(1000),
                        start_time time,
                        finish_time time,
                        course_id integer not null,
                        foreign key (course_id) references course(id)
);

create table student_course (
                                student_id bigint,
                                course_id bigint,
                                foreign key (student_id) references student(id),
                                foreign key (course_id) references course(id)
);

insert into student_course (student_id, course_id)
values (1,1),
       (1,2),
       (2,3),
       (4,3),
       (1,3),
       (2,4)


alter table lesson
    alter column course_id drop not null

