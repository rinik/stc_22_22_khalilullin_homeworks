insert into student (email, password, age, is_worker)
values ('rinat@gmail.com', 'qwerty001', 34, false);
insert into student (email, password, age, is_worker)
values ('kamila@mail.ru', 'qwerty002', 32, true);
insert into student (email, password, age, is_worker)
values ('mama@yandex.ru', 'qwerty003', 60, true);
insert into student (email, password, age, is_worker)
values ('papa@yahoo.com', 'qwerty004', 62, true);


update student
set first_name = 'Ринат', last_name = 'Халилуллин'
where id = 1;
update student
set first_name = 'Камила', last_name = 'Касумова'
where id = 2;
update student
set first_name = 'Равиля', last_name = 'Сафина'
where id = 3;
update student
set first_name = 'Зиннур', last_name = 'Халилуллин'
where id = 4;

insert into course (title, description, start, finish)
values ('Java', 'Разработка на Java', '2022-07-25', '2023-04-06');
insert into course (title, description, start, finish)
values ('PHP', 'Разработка на PHP', '2022-07-27', '2023-05-01')
insert into course (title, description, start, finish)
values ('SQL', 'Введение в работу с БД', '2022-07-25', '2023-11-29')
insert into course (title, description, start, finish)
values ('Spring', 'Разработка на Java с использованием Spring', '2020-01-25', '2022-01-15');

insert into lesson (name, summary, start_time, finish_time, course_id)
values ('ООП', 'Определение объекта и класса', '09:00', '12:00', 1),
       ('Java лучше PHP', 'Потому что...', '10:00', '15:00', 2),
       ('Индексы в БД', 'Есть btree', '10:00', '16:00', 3),
       ('Бины', 'Бины в Spring', '09:00', '15:00', 4);

insert into lesson (name, summary, start_time, finish_time, course_id)
values ('План запроса', 'Explain Analyze', '10:00', '17:00', 3);