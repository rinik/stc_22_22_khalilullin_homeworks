package ru.inno;

import java.util.Scanner;
public class ATM {

    public final static double MAX_AMOUNT_TO_WITHDRAW = 3000;
    public final static double MAX_AMOUNT_OF_MONEY = 5000;
    public final static double DEFAULT_AMOUNT_OF_MONEY = 4000;

    private double leftAmount;
    private int quantity = 0;

    ATM(double leftAmount){
        if (leftAmount >= 0) {
            if (leftAmount <= MAX_AMOUNT_OF_MONEY) {
                this.leftAmount = leftAmount;
            } else {
                this.leftAmount = DEFAULT_AMOUNT_OF_MONEY;
            }
        } else {
            this.leftAmount = 0;
        }
    }

    void give(){
        boolean i = true;
        double value;
        Scanner scanner = new Scanner(System.in);
        while (i){
            System.out.print("\nВведите сумму для выдачи наличных: ");
            value = scanner.nextDouble();
            if (value <= 0) {
                return;
            }
            if(value <= MAX_AMOUNT_TO_WITHDRAW && value <= leftAmount){
                System.out.println("\nВыдано " + value + "₽ \nСпасибо! До новых встреч!");
                leftAmount -= value;
                quantity++;
                i = false;
            } else if(value > MAX_AMOUNT_TO_WITHDRAW){
                System.out.println("Вы превысили лимит для выдачи средств (3 000₽)! Пожалуйста, введите меньшую сумму!");
            } else {
                System.out.println("Недостаточно средств для выдачи! Доступная сумма в банкомате: " + leftAmount + "₽ \n! Введите меньшую сумму!");
            }
        }
    }

    void pull(double value){
        if(value  <= 0) {
            return;
        }
        if(leftAmount + value <= MAX_AMOUNT_OF_MONEY){
            System.out.println("На счёт положено " + value + "₽ \nСпасибо! До новых встреч!");
            leftAmount += value;
        } else {
            double dntPull = value - (MAX_AMOUNT_OF_MONEY - leftAmount);
            System.out.println("Достигнут максимальный лимит! На счёт зачислено " + (value - dntPull) + "₽\n\nВозвращено средств " + dntPull + "₽\nСпасибо! До новых встреч! :)");
            leftAmount += (value - dntPull);
        }
        quantity++;
    }

    int getQuantity(){
        return this.quantity;
    }

    double getLeftAmount(){
        return this.leftAmount;
    }
}
