package ru.inno;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try {
            ATM atm = new ATM(50_000);

            atm.give();

            System.out.println("\nДоступная сумма в банкомате: " + atm.getLeftAmount() + "₽ \n");

            double valueToPull;
            System.out.print("Внесите наличные: ");
            valueToPull = scanner.nextDouble();

            atm.pull(valueToPull);

            System.out.println("\n\nКолличество проведённых операций: " + atm.getQuantity() + " \nВ банкомате осталось: " + atm.getLeftAmount() + "₽\n");
        } catch (InputMismatchException e) {
            System.err.println("Введена некорректная сумма! Пожалуйста, введите ЦЕЛОЧИСЛЕННОЕ значение");
        }
    }
}