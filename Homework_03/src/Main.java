import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите колличество элементов массива: ");
        int count = 0;
        int length = scanner.nextInt();
        int[] array = new int[length];
        for (int i = 0; i < length; i ++) {
            System.out.print("Значение " + i + "-го элемента: ");
            array [i] = scanner.nextInt();
        }
        if (array [0] < array [1]) {
            count++;
        }
        for (int i = 1; i < length - 1; i ++) {
            if (array [i - 1] > array [i] && array [i] < array [i + 1]) {
                count++;
            }
        }
        if (array [length - 2] > array [length - 1]) {
            count++;
        }
        System.out.println(count);

    }
}