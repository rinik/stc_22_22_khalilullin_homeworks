package ru.inno;

public interface ArrayTask {
    int resolve(int[] array, int from, int to);
}
