package ru.inno;

public class Main {
    public static void main(String[] args) {
        ArrayTask sum = (array,from,to) -> {
            int Sum =0;
            for(int i = from; i < to; i++ ){
                Sum += array[i];
            }
            return Sum;
        };
        ArrayTask sumMaxNumber = (array, from, to) -> {
            int max = array[from];
            for(int i = from; i < to; i++ ) {
                if (array[i] > max) {
                    max = array[i];
                }
            }
            System.out.println("Максимальное число в диапазоне " + max);
            int Sum = 0;
            while (max > 0){
                Sum += max % 10;
                max = max / 10;
            }
            return Sum;
        };
        int [] array = {4,7,61,32,11,4,2,8,9,11,76};
        ArraysTasksResolver.resolveTask(array,sum,2,9);
        ArraysTasksResolver.resolveTask(array,sumMaxNumber,2,9);
    }
}
